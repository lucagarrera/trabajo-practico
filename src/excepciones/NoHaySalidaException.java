/**
 * Esta excepción se arroja cuando el servidor recibe un mensaje de tipo
 * desconocido, ya que dependiendo del tipo de mensaje actúa de una forma
 * diferente y no definimos una forma predeterminada de actuar.
 */
package excepciones;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class NoHaySalidaException extends RuntimeException {

    public NoHaySalidaException() {
        super("No hay conexión con el servidor.");
    }
}
