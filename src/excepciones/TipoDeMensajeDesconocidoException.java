/**
 * Esta excepción se arroja cuando el servidor recibe un mensaje de tipo
 * desconocido, ya que dependiendo del tipo de mensaje actúa de una forma
 * diferente y no definimos una forma predeterminada de actuar.
 */
package excepciones;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class TipoDeMensajeDesconocidoException extends RuntimeException {

    public TipoDeMensajeDesconocidoException(String mensajeSerializado) {
        super(
                "Se recibió un mensaje de tipo desconocido. El mensaje recibido fue: "
                + mensajeSerializado
        );
    }
}
