/**
 * Esta excepción se arroja cuando se le solicita un nombre de usuario al
 * servidor, pero dicho nombre ya se encuentra utilizado.
 */
package excepciones;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class NombreDeUsuarioYaUsadoException extends RuntimeException {

    public NombreDeUsuarioYaUsadoException() {
        super("El nombre de usuario ya fue usado.");
    }

}
