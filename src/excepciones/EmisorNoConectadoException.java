/**
 * Esta excepción se arroja cuando el servidor recibe un mensaje de un usuario
 * que no está conectado.
 */
package excepciones;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class EmisorNoConectadoException extends RuntimeException {

    public EmisorNoConectadoException() {
        super("El emisor del mensaje no está conectado.");
    }

}
