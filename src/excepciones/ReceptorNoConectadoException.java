/**
 * Esta excepción se arroja cuando el servidor recibe un mensaje privado cuyo
 * destinatario no se encuentra conectado.
 */
package excepciones;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class ReceptorNoConectadoException extends RuntimeException {

    public ReceptorNoConectadoException() {
        super("El receptor del mensaje no está conectado.");
    }

}
