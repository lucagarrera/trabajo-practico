/**
 * Las instancias de esta clase representan solicitantes de nombres.
 */
package cliente.controladores;

import cliente.interfaz.componentes.InterfazDeSolicitanteDeNombres;
import cliente.modelos.ModeloDeSolicitanteDeNombres;
import java.io.IOException;
import java.net.Socket;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class ControladorDeSolicitanteDeNombres {

    public static String solicitarNombre(Socket socket)
            throws IOException {
        String nombre = InterfazDeSolicitanteDeNombres.solicitarNombre(true);
        while (ModeloDeSolicitanteDeNombres.yaEstaElegido(nombre, socket)) {
            nombre = InterfazDeSolicitanteDeNombres.solicitarNombre(false);
        }
        return nombre;
    }

}
