/**
 * Las instancias de esta clase representan clientes.
 */
package cliente.controladores;

import cliente.interfaz.componentes.InterfazDeCliente;
import cliente.modelos.ModeloDeCliente;
import mensajes.Mensaje;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public final class ControladorDeCliente {

    private ModeloDeCliente modelo;
    private InterfazDeCliente vista;
    private boolean cerrandose = false;

    public ControladorDeCliente(int puerto) {
        fijarModelo(new ModeloDeCliente(puerto));
        fijarVista(new InterfazDeCliente(puerto, 500, 600));
        obtenerModelo().fijarControlador(this);
        obtenerVista().fijarControlador(this);
    }

    public void conectarAlServidor() {
        obtenerModelo().conectarAlServidor();
    }

    public void cerrar() {
        cerrandose = true;
        obtenerModelo().cerrar();
        obtenerVista().cerrar();
    }

    public ModeloDeCliente obtenerModelo() {
        return modelo;
    }

    public String obtenerNombre() {
        return obtenerModelo().obtenerNombre();
    }

    public InterfazDeCliente obtenerVista() {
        return vista;
    }

    private void fijarModelo(ModeloDeCliente modeloDeCliente) {
        modelo = modeloDeCliente;
    }

    private void fijarVista(InterfazDeCliente interfazDeCliente) {
        vista = interfazDeCliente;
    }

    public void enviarMensaje(Mensaje mensaje) {
        obtenerModelo().enviarMensaje(mensaje);
    }

    public void mostrarMensajeDeError(String mensaje) {
        obtenerVista().mostrarMensajeDeError(mensaje);
    }

    public void mostrar() {
        obtenerVista().setTitle("Chatux - " + obtenerNombre());
        obtenerVista().setVisible(true);
    }

    public boolean corriendo() {
        return !cerrandose;
    }
}
