/**
 * Las instancias de esta clase representan el panel de mensajes de la interfaz
 * de un cliente. El objetivo de ellas es mostrar los mensajes recibidos por el
 * cliente.
 */
package cliente.interfaz.componentes;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class PanelDeMensajes extends JPanel implements Scrollable {

    private boolean scrollHorizontal;
    private boolean scrollVertical;

    public PanelDeMensajes(boolean scrollHorizontal, boolean scrollVertical) {
        fijarScrollHorizontal(scrollHorizontal);
        fijarScrollVertical(scrollVertical);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setOpaque(false);
    }

    public void agregarMensaje(
            String transmisor,
            String contenido,
            Color colorDeFondoDeContenido,
            boolean mensajePropio
    ) {
        add(new ContenedorDeMensaje(
                transmisor,
                contenido,
                colorDeFondoDeContenido,
                mensajePropio
        ));
    }

    private void fijarScrollHorizontal(boolean bool) {
        scrollHorizontal = bool;
    }

    private boolean obtenerScrollHorizontal() {
        return scrollHorizontal;
    }

    private void fijarScrollVertical(boolean bool) {
        scrollVertical = bool;
    }

    private boolean obtenerScrollVertical() {
        return scrollVertical;
    }

    //Implementación de Scrollable
    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle rectVisible,
            int orientacion,
            int direccion) {
        if (orientacion == SwingConstants.HORIZONTAL
                || orientacion == SwingConstants.VERTICAL) {
            return 70;
        }
        throw new IllegalArgumentException(
                "Orientacion invalida: " + orientacion
        );
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle rectVisible,
            int orientacion,
            int direccion) {
        if (orientacion == SwingConstants.HORIZONTAL) {
            return rectVisible.width;
        } else if (orientacion == SwingConstants.VERTICAL) {
            return rectVisible.height;
        }
        throw new IllegalArgumentException(
                "Orientacion invalida: " + orientacion
        );
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return !obtenerScrollHorizontal();
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return !obtenerScrollVertical();
    }
}
