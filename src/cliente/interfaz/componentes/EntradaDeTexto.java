/**
 * Esta clase permite que un componente de la interfaz sea desplazable mediante
 * una barra de desplazamiento.
 */
package cliente.interfaz.componentes;

import cliente.interfaz.constantes.Colores;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JTextField;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class EntradaDeTexto extends JTextField implements KeyListener {

    private InterfazDeCliente interfaz;

    public EntradaDeTexto(InterfazDeCliente interfaz) {
        fijarInterfaz(interfaz);
        setBackground(Colores.COLOR_DE_FONDO_DE_ENTRADA);
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Colores.COLOR_DE_BORDES),
                BorderFactory.createEmptyBorder(8, 8, 8, 8)
        ));
        setForeground(Colores.COLOR_DE_TEXTO);
        addKeyListener(this);
    }

    private void fijarInterfaz(InterfazDeCliente interfazDeCliente) {
        interfaz = interfazDeCliente;
    }

    private InterfazDeCliente obtenerInterfaz() {
        return interfaz;
    }

    public void enviarTexto() {
        obtenerInterfaz().enviarMensaje(getText());
        setText(""); //vacía el campo
        requestFocusInWindow(); // parpadea el cursor (?)
    }

    //implementación de KeyListener
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER && !getText().equals("")) {
            enviarTexto();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
