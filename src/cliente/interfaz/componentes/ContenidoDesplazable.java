/**
 * Esta clase permite que un componente de la interfaz sea desplazable mediante
 * una barra de desplazamiento.
 */
package cliente.interfaz.componentes;

import cliente.interfaz.constantes.Colores;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class ContenidoDesplazable extends JScrollPane {

    public ContenidoDesplazable(Component contenido) {
        super(contenido);
        setBorder(BorderFactory.createEmptyBorder());
        getViewport().setBackground(Colores.COLOR_DE_FONDO);
    }
}
