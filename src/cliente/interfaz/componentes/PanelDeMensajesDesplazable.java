/**
 * Esta clase permite que un componente de la interfaz sea desplazable mediante
 * una barra de desplazamiento.
 */
package cliente.interfaz.componentes;

import cliente.interfaz.constantes.Colores;
import java.awt.Adjustable;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class PanelDeMensajesDesplazable extends JScrollPane {

    private PanelDeMensajes contenido;

    private PanelDeMensajes obtenerContenido() {
        return contenido;
    }

    private void fijarContenido(PanelDeMensajes componente) {
        contenido = componente;
    }

    public PanelDeMensajesDesplazable(PanelDeMensajes contenido) {
        super(contenido);
        fijarContenido(contenido);
        setBorder(BorderFactory.createEmptyBorder());
        getViewport().setBackground(Colores.COLOR_DE_FONDO);
    }

    public boolean estaAbajo() {
        Adjustable scrollbar = getVerticalScrollBar();
        return scrollbar.getValue() + scrollbar.getVisibleAmount()
                == scrollbar.getMaximum();
    }

    public void moverHaciaAbajo() {
        Adjustable scrollbar = getVerticalScrollBar();
        scrollbar.setValue(scrollbar.getMaximum() - scrollbar.getVisibleAmount());
    }

    public void agregarMensaje(
            String infoDeMensaje,
            String contenido,
            Color colorDeFondo,
            boolean mensajePropio
    ) {
        obtenerContenido().agregarMensaje(
                infoDeMensaje,
                contenido,
                colorDeFondo,
                mensajePropio
        );
    }
;
}
