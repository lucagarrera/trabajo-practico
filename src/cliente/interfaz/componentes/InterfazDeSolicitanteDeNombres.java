/**
 * Las instancias de esta clase son interfaces gráficas que permiten ingresar
 * nombres de usuario.
 */
package cliente.interfaz.componentes;

import javax.swing.JOptionPane;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class InterfazDeSolicitanteDeNombres extends JOptionPane {

    private static final String TITULO = "Bienvenida/o";

    public static String solicitarNombre(boolean primeraVez) {
        String nombre = showInputDialog(
                null,
                primeraVez
                        ? "Ingrese su nombre de usuario:"
                        : "Ese nombre de usuario ya fue elegido. Ingrese otro nombre de usuario:",
                TITULO,
                DEFAULT_OPTION
        );
        while (nombre != null && (nombre.equals("") || nombre.contains(" "))) {
            nombre = showInputDialog(
                    null,
                    "Ese nombre de usuario es inválido, por favor ingrese otro nombre de usuario:",
                    TITULO,
                    DEFAULT_OPTION
            );
        } //además deberíamos chequear que el nombre no esté ya elegido...
        return nombre;
    }
}
