/**
 * Las instancias de esta clase representan el cuadro mediante el cual se muesta
 * un mensaje en la interfaz de un usuario.
 */
package cliente.interfaz.componentes;

import cliente.interfaz.constantes.Colores;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class ContenidoDeMensaje extends JLabel {

    public static final Font FUENTE = new Font(
            "Serif", Font.PLAIN, 15
    );

    public static final String SEPARADOR = "<br>";

    public ContenidoDeMensaje(String infoDeMensaje,
            String contenido,
            Color colorDeFondo
    ) {
        /**
         * Envuelvo el mensaje en tags html para que el texto se divida en la
         * cantidad de líneas necesarias automáticamente
         */
        super(
                "<html>"
                + (infoDeMensaje == null
                        ? ""
                        : infoDeMensaje + ContenidoDeMensaje.SEPARADOR)
                + contenido.replace("<", "&lt;")
                + "</html>");
        Font fuente = ContenidoDeMensaje.FUENTE;
        setOpaque(true);
        setBackground(colorDeFondo);
        setForeground(Colores.COLOR_DE_TEXTO);
        setBorder(new BordeDeContenidoDeMensaje(
                colorDeFondo
        ));
        if (fuente != null) {
            setFont(fuente);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(getWidth(), super.getPreferredSize().height);
    }

    @Override
    public int getWidth() {
        return Math.min(200, super.getPreferredSize().width);
    }
}
