/**
 * Esta clase tiene el propósito de reservar una fila en el panel de mensajes
 * de la interfaz de un cliente para la posterior alineación del contenido del
 * mensaje correspondiente.
 */
package cliente.interfaz.componentes;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class ContenedorDeMensaje extends JPanel {

    public static final Border BORDE_DE_CONTENEDOR
            = BorderFactory.createEmptyBorder(8, 8, 0, 8);

    public ContenedorDeMensaje(
            String infoDeMensaje,
            String contenido,
            Color colorDeFondoDeContenido,
            boolean mensajePropio
    ) {
        super();
        setLayout(new BorderLayout());
        setBorder(ContenedorDeMensaje.BORDE_DE_CONTENEDOR);
        setOpaque(false);
        ContenidoDeMensaje componente = new ContenidoDeMensaje(
                infoDeMensaje,
                contenido,
                colorDeFondoDeContenido
        );
        add(
                componente,
                mensajePropio ? BorderLayout.LINE_END : BorderLayout.LINE_START
        );
    }
}
