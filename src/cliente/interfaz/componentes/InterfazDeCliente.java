/**
 * Las instancias de esta clase representan la interfaz (o vista) de un cliente.
 */
package cliente.interfaz.componentes;

import cliente.controladores.ControladorDeCliente;
import cliente.interfaz.constantes.Colores;
import excepciones.NoHaySalidaException;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import mensajes.FabricaDeMensajes;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class InterfazDeCliente extends JFrame {

    private PanelDeMensajesDesplazable mensajes;
    private EntradaDeTexto entrada;
    private ControladorDeCliente controlador;

    public InterfazDeCliente(int puerto, int ancho, int alto) {
        prepararInterfaz(ancho, alto);
        pack();
    }

    private void prepararInterfaz(
            int ancho,
            int alto
    ) {
        EntradaDeTexto entrada;
        GridBagConstraints restricciones;
        Container contentPane = getContentPane();
        setMinimumSize(new Dimension(350, 350));
        setSize(new Dimension(ancho, alto));
        /**
         * No usar EXIT_ON_CLOSE ya que si abrieramos múltiples clientes en el
         * main, al cerrar uno se cerraría la aplicación, lo cual incluye a los
         * demás clientes. Además se puede agregar lógica personalizada mediante
         * la sobreescritura del método dispose.
         */
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        contentPane.setLayout(new GridBagLayout());
        contentPane.setBackground(Colores.COLOR_DE_FONDO);

        restricciones = new GridBagConstraints();
        restricciones.weightx = 1;
        restricciones.weighty = 1;
        restricciones.fill = GridBagConstraints.BOTH;
        restricciones.gridx = 0;
        restricciones.gridy = 0;
        restricciones.gridwidth = 2;

        fijarPanelDeMensajes(new PanelDeMensajesDesplazable(
                new PanelDeMensajes(false, true)
        ));
        add(obtenerPanelDeMensajes(), restricciones);

        restricciones = new GridBagConstraints();
        restricciones.weightx = 1;
        restricciones.weighty = 0;
        restricciones.insets = new Insets(8, 8, 8, 8);
        restricciones.fill = GridBagConstraints.HORIZONTAL;
        restricciones.gridx = 0;
        restricciones.gridy = 1;

        entrada = new EntradaDeTexto(this);
        add(entrada, restricciones);
        fijarEntrada(entrada);

        restricciones.weightx = 0;
        restricciones.weighty = 0;
        restricciones.insets = new Insets(0, 0, 0, 8);
        restricciones.fill = GridBagConstraints.HORIZONTAL;
        restricciones.gridx = 1;
        restricciones.gridy = 1;
        restricciones.gridwidth = 1;
        JButton botonEnviar = new JButton("Enviar");
        add(botonEnviar, restricciones);

        botonEnviar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                presionarEnviarMensaje(e);
            }
        });

    }

    public void mostrarMensaje(String usuario,
            String contenido,
            Color colorDeFondo,
            boolean mensajePropio) {
        PanelDeMensajesDesplazable panel = obtenerPanelDeMensajes();
        boolean estabaAbajo = panel.estaAbajo();
        panel.agregarMensaje(
                usuario,
                contenido,
                colorDeFondo,
                mensajePropio
        );
        pack();
        if (estabaAbajo) {
            panel.moverHaciaAbajo();
        }
    }

    public String obtenerNombre() {
        return obtenerControlador().obtenerNombre();
    }

    private void fijarPanelDeMensajes(PanelDeMensajesDesplazable panel) {
        mensajes = panel;
    }

    public PanelDeMensajesDesplazable obtenerPanelDeMensajes() {
        return mensajes;
    }

    private void fijarEntrada(EntradaDeTexto textField) {
        entrada = textField;
    }

    public EntradaDeTexto obtenerEntrada() {
        return entrada;
    }

    public void fijarControlador(ControladorDeCliente controladorDeCliente) {
        controlador = controladorDeCliente;
    }

    public ControladorDeCliente obtenerControlador() {
        return controlador;
    }

    public void enviarMensaje(String texto) {
        try {
            obtenerControlador().enviarMensaje(
                    FabricaDeMensajes.obtenerInstancia().obtenerMensaje(
                            obtenerNombre(),
                            texto
                    )
            );
        } catch (NoHaySalidaException excepcion) {
            mostrarMensajeDeError(excepcion.getMessage());
        }
    }

    public void cerrar() {
        super.dispose();
    }

    @Override
    public void dispose() {
        obtenerControlador().cerrar();
    }

    @Override
    public Dimension getPreferredSize() {
        return getSize();
    }

    public void mostrarMensajeDeError(String mensaje) {
        mostrarMensaje("ERROR", mensaje, Colores.COLOR_DE_ERROR, false);
    }

    //BOTONES
    public void presionarEnviarMensaje(ActionEvent e) {
        obtenerEntrada().enviarTexto();
    }
}
