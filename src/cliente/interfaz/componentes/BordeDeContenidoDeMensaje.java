/**
 * Esta clase tiene el propósito de darle bordes con puntas redondas a los
 * mensajes en la interfaz de un cliente.
 */
package cliente.interfaz.componentes;

import cliente.interfaz.constantes.Colores;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.Border;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class BordeDeContenidoDeMensaje implements Border {

    public static final int ANCHO_DE_BORDES_DE_MENSAJES = 8;
    /* El radio de los bordes de los mensajes debe ser
     * menor al ancho de sus bordes y mayor o igual a 0
     */
    public static final int RADIO_DE_BORDES_DE_MENSAJES = 8;

    private static final Insets ANCHO = new Insets(
            BordeDeContenidoDeMensaje.ANCHO_DE_BORDES_DE_MENSAJES,
            BordeDeContenidoDeMensaje.ANCHO_DE_BORDES_DE_MENSAJES,
            BordeDeContenidoDeMensaje.ANCHO_DE_BORDES_DE_MENSAJES,
            BordeDeContenidoDeMensaje.ANCHO_DE_BORDES_DE_MENSAJES
    );
    private Color colorDeFondo;

    public BordeDeContenidoDeMensaje(Color color) {
        fijarColorDeFondo(color);
    }

    @Override
    public void paintBorder(Component componente,
            Graphics graficos,
            int x,
            int y,
            int ancho,
            int alto) {
        graficos.setColor(Colores.COLOR_DE_FONDO);
        graficos.fillRect(
                x,
                y,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES
        );
        graficos.fillRect(
                x
                + ancho
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                y,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES
        );
        graficos.fillRect(
                x,
                y
                + alto
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES
        );
        graficos.fillRect(
                x
                + ancho
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                y
                + alto
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES
        );
        graficos.setColor(obtenerColorDeFondo());
        graficos.fillArc(
                x
                + ancho
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                y,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                0,
                90
        );
        graficos.fillArc(
                x,
                y,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                90,
                90
        );
        graficos.fillArc(
                x,
                y
                + alto
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                180,
                90
        );
        graficos.fillArc(
                x
                + ancho
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                y
                + alto
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                270,
                90
        );
        graficos.fillRect(
                x + BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                y,
                ancho
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES
        );
        graficos.fillRect(
                x,
                y + BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                alto - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2
        );
        graficos.fillRect(
                x + BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                y
                + alto
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                ancho
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES
        );
        graficos.fillRect(
                x
                + ancho
                - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                y + BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES,
                alto - BordeDeContenidoDeMensaje.RADIO_DE_BORDES_DE_MENSAJES * 2
        );
    }

    @Override
    public Insets getBorderInsets(Component componente) {
        return BordeDeContenidoDeMensaje.ANCHO;
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }

    private Color obtenerColorDeFondo() {
        return colorDeFondo;
    }

    private void fijarColorDeFondo(Color color) {
        colorDeFondo = color;
    }

}
