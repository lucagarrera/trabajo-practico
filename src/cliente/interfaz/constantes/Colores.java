/**
 * Esta clase define los diferentes colores de las interfaces de los usuarios.
 */
package cliente.interfaz.constantes;

import java.awt.Color;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class Colores {

    public static final Color COLOR_DE_TEXTO = Color.black;
    public static final Color COLOR_DE_FONDO = new Color(253, 241, 201);
    public static final Color COLOR_DE_FONDO_DE_MENSAJE = new Color(252, 191, 109);
    public static final Color COLOR_DE_FONDO_DE_ENTRADA = Color.white;
    public static final Color COLOR_DE_BORDES = Color.black;
    public static final Color COLOR_DE_ERROR = new Color(255, 43, 92);
    public static final Color COLOR_DE_PRIVADO = new Color(200, 201, 246);
    public static final Color COLOR_DE_CONEXION = new Color(96, 250, 111);
}
