/**
 * Esta clase contiene la funcionalidad de chequear si un nombre de usuario está
 * disponible para ser utilizado.
 */
package cliente.modelos;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import mensajes.MensajeDeSolicitudDeNombre;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class ModeloDeSolicitanteDeNombres {

    public static boolean yaEstaElegido(String nombre, Socket socket)
            throws IOException {
        BufferedReader entrada = new BufferedReader(
                new InputStreamReader(
                        socket.getInputStream(),
                        StandardCharsets.UTF_8
                )
        );
        new TransmisorDeMensajes(new PrintWriter(
                socket.getOutputStream(),
                true
        )).transmitirMensaje(new MensajeDeSolicitudDeNombre(nombre));
        return entrada.readLine().equals(":(");
    }
}
