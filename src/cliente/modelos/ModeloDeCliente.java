/**
 * Esta clase contiene la funcionalidad de conexión con el servidor, recepción y
 * emisión de mensajes de los clientes, aunque la implementación de la última
 * fue delegada en la clase TransmisorDeMensajes.
 */
package cliente.modelos;

import cliente.controladores.ControladorDeCliente;
import cliente.controladores.ControladorDeSolicitanteDeNombres;
import excepciones.NoHaySalidaException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import mensajes.FabricaDeMensajes;
import mensajes.Mensaje;
import mensajes.MensajeDeConexion;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class ModeloDeCliente extends Thread {

    private int puerto;
    private Socket socket;
    private TransmisorDeMensajes transmisor;
    private ControladorDeCliente controlador;
    private String nombre;
    private BufferedReader entrada;

    public String obtenerNombre() {
        return nombre;
    }

    private void fijarNombre(String string) {
        nombre = string;
    }

    public ModeloDeCliente(int puerto) {
        fijarPuerto(puerto);
    }

    public void conectarAlServidor() {
        try {
            byte[] ip = Files.readAllBytes(Paths.get("IP del servidor"));
            fijarSocket(new Socket(new String(ip).trim(), obtenerPuerto()));
            fijarSocket(socket);
            fijarTransmisor(new TransmisorDeMensajes(new PrintWriter(
                    socket.getOutputStream(),
                    true
            )));
            fijarNombre(
                    ControladorDeSolicitanteDeNombres.solicitarNombre(socket)
            );
            enviarMensaje(new MensajeDeConexion(obtenerNombre()));
            start();
        } catch (IOException excepcion) {
            obtenerControlador().mostrarMensajeDeError(
                    "Hubo un problema al conectarse al servidor."
            );
        }
    }

    public void enviarMensaje(Mensaje mensaje) {
        obtenerTransmisor().transmitirMensaje(mensaje);
    }

    private void fijarPuerto(int numero) {
        puerto = numero;
    }

    private void fijarTransmisor(TransmisorDeMensajes transmisorDeMensajes) {
        transmisor = transmisorDeMensajes;
    }

    private int obtenerPuerto() {
        return puerto;
    }

    private void fijarSocket(Socket s) {
        socket = s;
    }

    private Socket obtenerSocket() {
        return socket;
    }

    public void cerrar() {
        try {
            Socket socket = obtenerSocket();
            if (socket != null) {
                obtenerEntrada().close();
                socket.close();
            }
        } catch (IOException excepcion) {
            obtenerControlador().mostrarMensajeDeError(excepcion.getMessage());
        }
    }

    private TransmisorDeMensajes obtenerTransmisor() {
        if (transmisor == null) {
            throw new NoHaySalidaException();
        }
        return transmisor;
    }

    private ControladorDeCliente obtenerControlador() {
        return controlador;
    }

    public void fijarControlador(ControladorDeCliente controladorDeCliente) {
        controlador = controladorDeCliente;
    }

    private BufferedReader obtenerEntrada() {
        return entrada;
    }

    public void fijarEntrada(BufferedReader bufferedReader) {
        entrada = bufferedReader;
    }

    @Override
    public void run() {
        try {
            BufferedReader entrada = new BufferedReader(
                    new InputStreamReader(
                            obtenerSocket().getInputStream(),
                            StandardCharsets.UTF_8
                    )
            );
            FabricaDeMensajes fabrica = FabricaDeMensajes.obtenerInstancia();
            Mensaje mensaje = fabrica.obtenerMensaje(entrada.readLine());
            fijarEntrada(entrada);
            if (mensaje != null) {
                mensaje.mostrar(obtenerControlador().obtenerVista());
            }
            while (mensaje != null) {
                if (entrada.ready()) {
                    mensaje = fabrica.obtenerMensaje(entrada.readLine());
                    if (mensaje != null) {
                        mensaje.mostrar(obtenerControlador().obtenerVista());
                    }
                }
            }
        } catch (IOException excepcion) {
            /**
             * Cuando la entrada se cierra, entrada.ready() arroja una excepción
             * y se sale del while (aunque no es la única causa posible de esta
             * excepción).
             */
            ControladorDeCliente cliente = obtenerControlador();
            if (!cliente.corriendo()) {
                cliente.mostrarMensajeDeError(excepcion.getMessage());
            }
        }
    }
}
