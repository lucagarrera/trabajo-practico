/**
 * Esta clase contiene la implementación de la emisión de los mensajes que le
 * envía un cliente a un servidor.
 */
package cliente.modelos;

import excepciones.NoHaySalidaException;
import java.io.PrintWriter;
import mensajes.Mensaje;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class TransmisorDeMensajes {

    private PrintWriter salida;

    public TransmisorDeMensajes(PrintWriter salida) {
        fijarSalida(salida);
    }

    public void transmitirMensaje(Mensaje mensaje) {
        if (mensaje == null) {
            obtenerSalida().println(mensaje);
        } else {
            obtenerSalida().println(mensaje.serializar());
        }
    }

    private void fijarSalida(PrintWriter printWriter) {
        salida = printWriter;
    }

    private PrintWriter obtenerSalida() {
        if (salida == null) {
            throw new NoHaySalidaException();
        }
        return salida;
    }
}
