/**
 * Las instancias de esta clase representan a los mensajes se envían a los
 * clientes cuando uno de ellos se desconecta del servidor.
 */
package mensajes;

import cliente.interfaz.componentes.InterfazDeCliente;
import cliente.interfaz.constantes.Colores;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class MensajeDeDesconexion extends Mensaje {

    public MensajeDeDesconexion(String emisor) {
        super(emisor);
    }

    @Override
    public void mostrar(InterfazDeCliente interfaz) {
        String emisor = obtenerNombreDeEmisor();
        interfaz.mostrarMensaje(
                null,
                emisor + " se desconectó.",
                Colores.COLOR_DE_CONEXION,
                emisor.equals(interfaz.obtenerNombre())
        );
    }

    @Override
    public String serializar() {
        String separador = FabricaDeMensajes.SEPARADOR;
        return FabricaDeMensajes.DESCONEXION
                + separador
                + serializarParte(obtenerNombreDeEmisor())
                + separador;
    }

}
