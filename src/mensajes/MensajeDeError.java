/**
 * Las instancias de esta clase representan a los mensajes de error.
 */
package mensajes;

import cliente.interfaz.componentes.InterfazDeCliente;
import java.util.Map;
import servidor.Usuario;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class MensajeDeError extends Mensaje {

    private String contenido;

    public MensajeDeError(String emisor, String contenido) {
        super(emisor);
        fijarContenido(contenido);
    }

    private void fijarContenido(String contenido) {
        this.contenido = contenido;
    }

    private String obtenerContenido() {
        return contenido;
    }

    @Override
    public String serializar() {
        String separador = FabricaDeMensajes.SEPARADOR;
        return FabricaDeMensajes.ERROR
                + separador
                + serializarParte(obtenerNombreDeEmisor())
                + separador
                + serializarParte(obtenerContenido())
                + separador;
    }

    @Override
    public void mostrar(InterfazDeCliente interfaz) {
        interfaz.mostrarMensajeDeError(contenido);
    }

    @Override
    public void enviar(Map<String, Usuario> usuarios) {
        usuarios.get(obtenerNombreDeEmisor()).getSalida().println(serializar());
    }
}
