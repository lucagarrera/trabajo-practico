/**
 * Las instancias de esta clase representan a un mensaje genérico.
 */
package mensajes;

import cliente.interfaz.componentes.InterfazDeCliente;
import excepciones.EmisorNoConectadoException;
import java.util.Map;
import servidor.ConexionServidorCliente;
import servidor.Usuario;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public abstract class Mensaje {

    private String nombreDeEmisor;

    public Mensaje(String emisor) {
        fijarNombreDeEmisor(emisor);
    }

    public String obtenerNombreDeEmisor() {
        return nombreDeEmisor;
    }

    private void fijarNombreDeEmisor(String nombre) {
        nombreDeEmisor = nombre;
    }

    protected String serializarParte(String parte) {
        String marcador = FabricaDeMensajes.MARCADOR;
        String separador = FabricaDeMensajes.SEPARADOR;
        return parte.replace(marcador, marcador + marcador)
                .replace(separador, marcador + separador);
    }

    public abstract void mostrar(InterfazDeCliente interfaz);

    public boolean validarUsuarios(ConexionServidorCliente conexion) {
        if (!conexion.estaConectado(obtenerNombreDeEmisor())) {
            throw new EmisorNoConectadoException();
        }
        return true;
    }

    public void enviar(Map<String, Usuario> usuarios) {
        for (Usuario receptor : usuarios.values()) {
            receptor.getSalida().println(serializar());
        }
    }

    public abstract String serializar();
}
