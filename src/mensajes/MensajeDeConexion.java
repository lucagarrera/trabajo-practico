/**
 * Las instancias de esta clase representan a los mensajes que los clientes
 * envían al conectarse al servidor.
 */
package mensajes;

import cliente.interfaz.componentes.InterfazDeCliente;
import cliente.interfaz.constantes.Colores;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class MensajeDeConexion extends Mensaje {

    public MensajeDeConexion(String emisor) {
        super(emisor);
    }

    @Override
    public void mostrar(InterfazDeCliente interfaz) {
        String emisor = obtenerNombreDeEmisor();
        interfaz.mostrarMensaje(
                null,
                emisor + " se conectó.",
                Colores.COLOR_DE_CONEXION,
                emisor.equals(interfaz.obtenerNombre())
        );
    }

    @Override
    public String serializar() {
        String separador = FabricaDeMensajes.SEPARADOR;
        return FabricaDeMensajes.CONEXION
                + separador
                + serializarParte(obtenerNombreDeEmisor())
                + separador;
    }

}
