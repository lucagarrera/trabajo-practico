/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensajes;

import cliente.interfaz.componentes.InterfazDeCliente;
import java.util.Map;
import servidor.ConexionServidorCliente;
import servidor.Usuario;

/**
 *
 * @author luca
 */
public class MensajeDeSolicitudDeNombre extends Mensaje {

    public MensajeDeSolicitudDeNombre(String emisor) {
        super(emisor);
    }

    @Override
    public void mostrar(InterfazDeCliente interfaz) {

    }

    @Override
    public String serializar() {
        String separador = FabricaDeMensajes.SEPARADOR;
        return FabricaDeMensajes.SOLICITUD
                + separador
                + serializarParte(obtenerNombreDeEmisor())
                + separador;
    }

    @Override
    public void enviar(Map<String, Usuario> usuarios) {

    }

    @Override
    public boolean validarUsuarios(ConexionServidorCliente conexion) {
        String emisor = obtenerNombreDeEmisor();
        if (conexion.estaConectado(emisor)) {
            conexion.rechazarUsuario();
        } else {
            conexion.aceptarUsuario(emisor);
        }
        return true;
    }
}
