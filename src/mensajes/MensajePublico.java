/**
 * Las instancias de esta clase representan a los mensajes públicos (es decir,
 * que todos los usuarios conectados al servidor al que este fue enviado pueden
 * leerlo).
 */
package mensajes;

import cliente.interfaz.componentes.InterfazDeCliente;
import cliente.interfaz.constantes.Colores;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class MensajePublico extends Mensaje {

    private String contenido;

    public MensajePublico(String emisor, String contenido) {
        super(emisor);
        fijarContenido(contenido);
    }

    private void fijarContenido(String contenido) {
        this.contenido = contenido;
    }

    private String obtenerContenido() {
        return contenido;
    }

    @Override
    public String serializar() {
        String separador = FabricaDeMensajes.SEPARADOR;
        return FabricaDeMensajes.PUBLICO
                + separador
                + serializarParte(obtenerNombreDeEmisor())
                + separador
                + serializarParte(obtenerContenido())
                + separador;
    }

    @Override
    public void mostrar(InterfazDeCliente interfaz) {
        interfaz.mostrarMensaje(
                obtenerNombreDeEmisor(),
                obtenerContenido(),
                Colores.COLOR_DE_FONDO_DE_MENSAJE,
                interfaz.obtenerNombre().equals(obtenerNombreDeEmisor())
        );
    }
}
