/**
 * Las instancias de esta clase representan a los mensajes privados (es decir,
 * que sólo pueden ser leídos por el usuario especificado).
 */
package mensajes;

import cliente.interfaz.componentes.InterfazDeCliente;
import cliente.interfaz.constantes.Colores;
import java.util.Map;
import servidor.ConexionServidorCliente;
import servidor.Usuario;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class MensajePrivado extends Mensaje {

    private String nombreDeReceptor;
    private String contenido;

    public MensajePrivado(
            String emisor,
            String receptor,
            String contenido
    ) {
        super(emisor);
        fijarContenido(contenido);
        fijarNombreDeReceptor(receptor);
    }

    @Override
    public boolean validarUsuarios(ConexionServidorCliente conexion) {
        boolean valido = super.validarUsuarios(conexion);
        if (!conexion.estaConectado(obtenerNombreDeReceptor())) {
            valido = false;
        }
        return valido;
    }

    public String obtenerNombreDeReceptor() {
        return nombreDeReceptor;
    }

    private void fijarNombreDeReceptor(String nombre) {
        nombreDeReceptor = nombre;
    }

    private void fijarContenido(String contenido) {
        this.contenido = contenido;
    }

    private String obtenerContenido() {
        return contenido;
    }

    @Override
    public String serializar() {
        String separador = FabricaDeMensajes.SEPARADOR;
        return FabricaDeMensajes.PRIVADO
                + separador
                + serializarParte(obtenerNombreDeEmisor())
                + separador
                + serializarParte(obtenerNombreDeReceptor())
                + separador
                + serializarParte(obtenerContenido())
                + separador;
    }

    @Override
    public void mostrar(InterfazDeCliente interfaz) {
        boolean mensajePropio = interfaz.obtenerNombre().equals(
                obtenerNombreDeEmisor()
        );
        interfaz.mostrarMensaje(
                mensajePropio
                        ? "Para " + obtenerNombreDeReceptor()
                        : obtenerNombreDeEmisor(),
                obtenerContenido(),
                Colores.COLOR_DE_PRIVADO,
                mensajePropio
        );
    }

    @Override
    public void enviar(Map<String, Usuario> usuarios) {
        usuarios.get(obtenerNombreDeEmisor()).getSalida().println(
                serializar()
        );
        usuarios.get(obtenerNombreDeReceptor()).getSalida().println(
                serializar()
        );
    }
}
