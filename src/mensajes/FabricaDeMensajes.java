/**
 * La instancia de esta clase (sólo se puede crear una) se encarga de crear
 * objetos de tipo Mensaje, instanciando las clases que extienden a Mensaje
 * según la lógica definida en esta clase.
 */
package mensajes;

import excepciones.TipoDeMensajeDesconocidoException;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class FabricaDeMensajes {

    /* Las propiedades estáticas de tipo String de esta clase no deben contener
     * caracteres especiales para las regex
     */
    public static final String PUBLICO = "0";
    public static final String PRIVADO = "1";
    public static final String CONEXION = "2";
    public static final String DESCONEXION = "3";
    public static final String SOLICITUD = "4";
    public static final String ERROR = "-1";
    public static final String MARCADOR = "/";
    public static final String SEPARADOR = ":";
    private static FabricaDeMensajes instancia;

    private FabricaDeMensajes() {

    }

    public static FabricaDeMensajes obtenerInstancia() {
        if (instancia == null) {
            instancia = new FabricaDeMensajes();
        }
        return instancia;
    }

    private String obtenerParteDeMensajeSerializado(int parte, String mensaje) {
        String patron = ".*";
        String marcador = FabricaDeMensajes.MARCADOR;
        String separador = FabricaDeMensajes.SEPARADOR;
        String patronDeParte
                = "(.*?(?:[^"
                + marcador
                + "]|"
                + marcador
                + marcador
                + "))"
                + separador;
        for (int i = 0; i < parte; i++) {
            patron = patronDeParte + patron;
        }
        return mensaje.replaceAll(patron, "$" + Integer.toString(parte)).
                replace(marcador + marcador, marcador).
                replace(marcador + separador, separador);
    }

    //devuelve un mensaje a partir de un mensaje serializado
    public Mensaje obtenerMensaje(String mensajeSerializado) {
        try {
            if (mensajeSerializado != null) {
                String tipo = obtenerParteDeMensajeSerializado(
                        1,
                        mensajeSerializado
                );
                Mensaje mensaje = null;
                if (tipo.equals(FabricaDeMensajes.PUBLICO)) {
                    mensaje = new MensajePublico(
                            obtenerParteDeMensajeSerializado(
                                    2,
                                    mensajeSerializado
                            ),
                            obtenerParteDeMensajeSerializado(
                                    3,
                                    mensajeSerializado
                            )
                    );
                } else if (tipo.equals(FabricaDeMensajes.PRIVADO)) {
                    mensaje = new MensajePrivado(
                            obtenerParteDeMensajeSerializado(
                                    2,
                                    mensajeSerializado
                            ),
                            obtenerParteDeMensajeSerializado(
                                    3,
                                    mensajeSerializado
                            ),
                            obtenerParteDeMensajeSerializado(
                                    4,
                                    mensajeSerializado
                            )
                    );
                } else if (tipo.equals(FabricaDeMensajes.CONEXION)) {
                    mensaje = new MensajeDeConexion(
                            obtenerParteDeMensajeSerializado(
                                    2,
                                    mensajeSerializado
                            )
                    );
                } else if (tipo.equals(FabricaDeMensajes.DESCONEXION)) {
                    mensaje = new MensajeDeDesconexion(
                            obtenerParteDeMensajeSerializado(
                                    2,
                                    mensajeSerializado
                            )
                    );
                } else if (tipo.equals(FabricaDeMensajes.SOLICITUD)) {
                    mensaje = new MensajeDeSolicitudDeNombre(
                            obtenerParteDeMensajeSerializado(2, mensajeSerializado)
                    );
                } else if (tipo.equals(FabricaDeMensajes.ERROR)) {
                    mensaje = new MensajeDeError(
                            obtenerParteDeMensajeSerializado(2, mensajeSerializado),
                            obtenerParteDeMensajeSerializado(3, mensajeSerializado)
                    );
                } else {
                    throw new TipoDeMensajeDesconocidoException(
                            mensajeSerializado
                    );
                }
                return mensaje;
            }
        } catch (IndexOutOfBoundsException excepcion) {
            System.out.println("El formato del mensaje es incorrecto.");
            System.out.println(excepcion.getMessage());
        } catch (TipoDeMensajeDesconocidoException excepcion) {
            System.out.println(excepcion.getMessage());
        }
        return null;
    }

    //devuelve un mensaje público o privado a partir del emisor de dicho mensaje
    //y del contenido del mismo
    public Mensaje obtenerMensaje(String emisor, String contenido) {
        try {
            String patron = "@(.*?)\\s(.*)";
            Mensaje mensaje = null;
            if (contenido.matches(patron)) {
                mensaje = new MensajePrivado(
                        emisor,
                        contenido.replaceAll(patron, "$1"),
                        contenido.replaceAll(patron, "$2")
                );
            } else {
                mensaje = new MensajePublico(emisor, contenido);
            }
            return mensaje;
        } catch (IndexOutOfBoundsException excepcion) {
            System.out.println("El formato del mensaje es incorrecto.");
            System.out.println(excepcion.getMessage());
        } catch (NullPointerException excepcion) {
            System.out.println(
                    "No se pudo determinar el contenido del mensaje."
            );
            System.out.println(excepcion.getMessage());
        }
        return null;
    }
}
