/**
 * Esta clase permite abrir (es el término correcto?) un servidor.
 */
package entrypoints;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class Servidor {

    public static void main(String[] args) {
        //hacer controlador y vista de servidor
        new servidor.Servidor(2345).start();
    }

}
