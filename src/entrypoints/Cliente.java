/**
 * Esta clase permite abrir (es el término correcto?) un cliente.
 */
package entrypoints;

import cliente.controladores.ControladorDeCliente;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class Cliente {

    public static void main(String[] args) {
        ControladorDeCliente cliente = new ControladorDeCliente(2345);
        cliente.conectarAlServidor();
        cliente.mostrar();
    }

}
