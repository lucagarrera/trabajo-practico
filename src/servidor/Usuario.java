/**
 * Las instancias de esta clase representan a un usuario.
 */
package servidor;

import java.io.PrintWriter;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class Usuario {

    private String nombre;
    private PrintWriter salida;

    public Usuario(String nombre, PrintWriter salida) {
        setNombre(nombre);
        setSalida(salida);
    }

    public String getNombre() {
        return nombre;
    }

    public PrintWriter getSalida() {
        return salida;
    }

    private void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private void setSalida(PrintWriter salida) {
        this.salida = salida;
    }

}
