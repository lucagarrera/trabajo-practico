/**
 * Cada instancia de esta clase se encarga de recibir mensajes de un único
 * cliente y de reenviarlos a los clientes adecuados (aunque la implementación
 * del reenvío fue delegada en las clases que extienden a la clase Mensaje).
 */
package servidor;

import excepciones.EmisorNoConectadoException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import mensajes.FabricaDeMensajes;
import mensajes.Mensaje;
import mensajes.MensajeDeDesconexion;
import mensajes.MensajeDeError;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class ConexionServidorCliente extends Thread {

    private BufferedReader entrada;
    private PrintWriter salida;
    private Map<String, Usuario> usuarios;
    private Socket socket;

    public ConexionServidorCliente(
            Socket socket,
            Map<String, Usuario> usuarios
    ) {
        try {
            fijarEntrada(new BufferedReader(new InputStreamReader(
                    socket.getInputStream(),
                    StandardCharsets.UTF_8
            )));
            fijarSalida(
                    new PrintWriter(socket.getOutputStream(), true)
            );
            fijarUsuarios(usuarios);
            fijarSocket(socket);
        } catch (IOException excepcion) {
            System.out.println(excepcion.getMessage());
        }
    }

    @Override
    public void run() {
        String emisor = null;
        try {
            FabricaDeMensajes fabrica = FabricaDeMensajes.obtenerInstancia();
            Mensaje mensaje = fabrica.obtenerMensaje(
                    obtenerEntrada().readLine()
            );
            Map<String, Usuario> usuarios = obtenerUsuarios();
            while (mensaje != null) {
                emisor = mensaje.obtenerNombreDeEmisor();
                if (mensaje.validarUsuarios(this)) {
                    mensaje.enviar(usuarios);
                }
                mensaje = fabrica.obtenerMensaje(obtenerEntrada().readLine());
            }
        } catch (IOException excepcion) {
            System.out.println(
                    "Ocurrió un error al recibir un mensaje de un cliente."
            );
            System.out.println(excepcion.getMessage());
        } catch (Exception excepcion) {
            try {
                System.out.println(excepcion.getMessage());
                if (estaConectado(emisor)) {
                    new MensajeDeError(
                            emisor,
                            excepcion.getMessage()
                    ).enviar(usuarios);
                }
            } catch (EmisorNoConectadoException excepcionDeMensajeDeError) {
                System.out.println(excepcionDeMensajeDeError.getMessage());
            }
        } finally {
            desconectarUsuario(emisor);
        }
    }

    private void conectarUsuario(String usuario) {
        obtenerUsuarios().putIfAbsent(usuario, new Usuario(
                usuario,
                obtenerSalida()
        ));
    }

    private void desconectarUsuario(String usuario) {
        try {
            Map<String, Usuario> usuarios = obtenerUsuarios();
            usuarios.remove(usuario);
            new MensajeDeDesconexion(usuario).enviar(usuarios);
            obtenerSocket().close();
        } catch (IOException excepcion) {
            System.out.println("Hubo un error al cerrar un socket.");
            System.out.println(excepcion.getMessage());
        }
    }

    public boolean estaConectado(String usuario) {
        return obtenerUsuarios().containsKey(usuario);
    }

    public void aceptarUsuario(String usuario) {
        conectarUsuario(usuario);
        obtenerSalida().println(":)"); //(?)
    }

    public void rechazarUsuario() {
        obtenerSalida().println(":("); //(?)
    }

    private PrintWriter obtenerSalida() {
        return salida;
    }

    private void fijarSalida(PrintWriter printWriter) {
        salida = printWriter;
    }

    private BufferedReader obtenerEntrada() {
        return entrada;
    }

    private void fijarEntrada(BufferedReader bufferedReader) {
        entrada = bufferedReader;
    }

    private Socket obtenerSocket() {
        return socket;
    }

    private void fijarSocket(Socket s) {
        socket = s;
    }

    private Map<String, Usuario> obtenerUsuarios() {
        return usuarios;
    }

    private void fijarUsuarios(Map<String, Usuario> coleccionDeUsuarios) {
        usuarios = coleccionDeUsuarios;
    }
}
