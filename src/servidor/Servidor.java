/**
 * Las instancias de esta clase representan a un servidor.
 */
package servidor;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * @author Brenda Miranda Chávez, Luca Garrera, Oriana Photiades
 */
public class Servidor extends Thread {

    private int puerto;
    private Map<String, Usuario> usuarios;

    public Servidor(int puerto) {
        fijarPuerto(puerto);
        inicializarUsuarios();
    }

    @Override
    public void run() {
        try {
            ServerSocket socketDelServidor = new ServerSocket(puerto);
            while (true) {
                new ConexionServidorCliente(
                        socketDelServidor.accept(),
                        obtenerUsuarios()
                ).start();
            }
        } catch (IOException excepcion) {
            System.out.println(
                    "Hubo un error mientras se intentaba escuchar al puerto "
                    + Integer.toString(obtenerPuerto()) + "."
            );
            System.out.println(excepcion.getMessage());
        }
    }

    private void inicializarUsuarios() {
        usuarios = new HashMap();
    }

    private Map<String, Usuario> obtenerUsuarios() {
        return usuarios;
    }

    private void fijarPuerto(int numero) {
        puerto = numero;
    }

    private int obtenerPuerto() {
        return puerto;
    }
}
